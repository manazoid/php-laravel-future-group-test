<?php

namespace App\Http\Controllers;

use App\Models\Notebook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class NotebookController extends Controller
{
    private function all() {
        return Notebook::orderByDesc("id");
    }
    /**
     * Display a listing of the resource.
     */
    public function index($limit = 10, $offset = 0)
    {
        return $this->all()->offset($offset)->limit($limit)->get();
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "full_name" => "required|string",
            "company" => "nullable|string",
            "phone" => "nullable|string",
            "email" => "required|email",
            "dob" => "required|date",
            'photo' => 'required|mimes:jpg,jpeg,png,bmp,tiff|max:4096',
        ], $messages = [
            'mimes' => 'please insert image only',
            'max' => 'image should be less than 4 MB'
        ]);

        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }
        $validated = $validator->validated();

        $tempURL = $request->file("photo")->store("public");
        $url = str_replace("public", "/storage", $tempURL);
        if (!$url) {
            return response()->json(["error" => "can not store image"]);
        }
        $validated["photo"] = $url;

        $notebook = Notebook::create($validated);
        return $notebook;
    }

    /**
     * Display the specified resource.
     */
    public function show(Notebook $notebook)
    {
        return $notebook;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Notebook $notebook)
    {
        $validator = Validator::make($request->all(), [
            "full_name" => "nullable|string",
            "company" => "nullable|string",
            "phone" => "nullable|string",
            "email" => "nullable|email",
            "dob" => "nullable|date",
            'photo' => 'nullable|mimes:jpg,jpeg,png,bmp,tiff|max:4096',
        ], $messages = [
            'mimes' => 'please insert image only',
            'max' => 'image should be less than 4 MB'
        ]);

        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }
        $validated = $validator->validated();

        $tempURL = $request->file("photo")->store("public") ?? '';
        $url = str_replace("public", "/storage", $tempURL);
        if (!isset($url) || !$url) {
            return response()->json(["error" => "can not store image"]);
        }
        $validated["photo"] = $url;

        if ($notebook->update($validated)) {
            return response()->json(["message" => "notebook was successfully updated"]);
        }

        return response()->json(["message" => "can not update the notebook now. try again later"], 400);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Notebook $notebook)
    {
        if ($notebook->delete()) {
            return response()->json(["message" => "notebook was successfully deleted"]);
        }

        return response()->json(["message" => "can not delete the notebook now. try again later"], 500);
    }
}
