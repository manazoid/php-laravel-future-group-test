<?php

namespace Database\Seeders;

use App\Models\Notebook;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TestNotebook extends Seeder
{
    /**
     * Run the database seeds.
     */
    // Profile photo of random persons
    private string $url = "https://randomuser.me/api/portraits/";
    private string $men = "men/";
    private string $women = "women/";
    private string $ext = ".jpg";
    private array $companies = [
        "Future Group", "Google", "Yandex",
    ];

    public function run(): void
    {
        $notebooks = [
            // Иностранные Васи Пупкины
            //
            // http://coollingua.blogspot.com/2011/12/blog-post_29.html
            ["full_name" => "Admin Admin", "email" => "admin@admin.ru"],
            ["full_name" => "Joe Bloggs", "email" => "joe_bloggs@example.com"],
            ["full_name" => "Bob Soap", "email" => "bob_soap@example.com"],
            ["full_name" => "Charlie Farnsbarns", "email" => "charlie_farnsbarns@example.com"],
            ["full_name" => "John Doe", "email" => "john_doe@example.com"],
            ["full_name" => "David Cohen", "email" => "david_cohen@example.com"],
            ["full_name" => "Tommy Atkins", "email" => "tommy_atkins@example.com"],
            ["full_name" => "David Soap", "email" => "david_soap@example.com"],
            ["full_name" => "Joe Snuffy", "email" => "joe_snuffy@example.com"],
            ["full_name" => "Sally Harry", "email" => "sally_harry@example.com",
                "photo" => $this->url.$this->women.mt_rand(1,99).$this->ext],
        ];

        foreach ($notebooks as $key => $notebook) {
            Notebook::create([
                "full_name" => $notebook["full_name"],
                "company" => $this->companies[mt_rand(0,2)],
                "phone" => "7" . mt_rand(9000000000, 9999999999),
                "email" => $notebook["email"],
                "dob" => Carbon::now()->subYears($key + 20),
                "photo" => $notebook["photo"] ?? $this->url.$this->men.mt_rand(1,99).$this->ext
            ]);
        }
    }
}
