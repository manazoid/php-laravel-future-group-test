<?php

use App\Http\Controllers\NotebookController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix("notebook")->group(function () {
    Route::get("/{limit}/{offset}", [NotebookController::class, "index"]);
    Route::post("/", [NotebookController::class, "store"]);
    Route::get("/{notebook}", [NotebookController::class, "show"]);
    Route::post("/{notebook}", [NotebookController::class, "update"]);
    Route::delete("/{notebook}", [NotebookController::class, "destroy"]);
});
